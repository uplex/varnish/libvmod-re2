# -D MUST pass in _version and _release, and SHOULD pass in dist.

Summary: Google RE2 regular expression module for Varnish Cache
Name: vmod-re2
Version: %{_version}
Release: %{_release}%{?dist}
License: BSD
Group: System Environment/Daemons
URL: https://code.uplex.de/uplex-varnish/libvmod-re2
Source0: %{name}-%{version}.tar.gz

# varnish from varnish61 at packagecloud
# This is the Requires for VMOD ABI compatibility with VRT >= 8.0.
Requires: varnishd(vrt)%{?_isa} >= 8
Requires: re2 >= 20150501

BuildRequires: varnish-devel >= 6.1.0
BuildRequires: re2-devel >= 20150501
BuildRequires: pkgconfig
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: python-docutils >= 0.6

# git builds
#BuildRequires: automake
#BuildRequires: autoconf
#BuildRequires: autoconf-archive
#BuildRequires: libtool
#BuildRequires: python-docutils >= 0.6

Provides: vmod-re2, vmod-re2-debuginfo

%description
Varnish Module (VMOD) for access to the Google RE2 regular expression
engine.

%prep
%setup -q -n %{name}-%{version}

%build

# if this were a git build
# ./autogen.sh

%configure

make -j

%check

make -j check

%install

make install DESTDIR=%{buildroot}

# Only use the version-specific docdir created by %doc below
rm -rf %{buildroot}%{_docdir}

# None of these for fedora/epel
find %{buildroot}/%{_libdir}/ -name '*.la' -exec rm -f {} ';'
find %{buildroot}/%{_libdir}/ -name '*.a' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/varnish*/vmods/
%{_mandir}/man3/*.3*
%doc README.rst LICENSE

%post
/sbin/ldconfig

%changelog
* Mon Dec 3 2018 Geoff Simmons <geoff@uplex.de> - 1.6.1
- True dependency on VRT 8.0, compatible with Varnish 6.1, libvarnishapi.so.2

* Fri Nov 30 2018 Geoff Simmons <geoff@uplex.de> - 1.5.2
- Strictly require Varnish 6.0 -- compatible with libvarnishapi.so.1

* Wed Oct 3 2018 Geoff Simmons <geoff@uplex.de> - 1.6.0
- Build for VRT 8.0 / Varnish 6.1.

* Wed Oct 3 2018 Geoff Simmons <geoff@uplex.de> - 1.5.1
- Require ABI compatibility -- also compatible with Varnish 6.0.1.
- RPM builds use make -j.

* Mon May 7 2018 Geoff Simmons <geoff@uplex.de> - 1.5.0
  Bugfix -- off-by-one error in rewrites

* Sun Apr 1 2018 Geoff Simmons <geoff@uplex.de> - 1.4.4
  Compatibility with Varnish 6.0.

* Tue Dec 19 2017 Geoff Simmons <geoff@uplex.de> - 1.4.3
  Enforce ABI compatibility with VRT 6.0.
