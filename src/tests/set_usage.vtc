# looks like -*- vcl -*-

varnishtest "legal usage of the set object interface"

varnish v1 -vcl { backend b None; } -start

varnish v1 -errvcl {vmod re2 failure: s.add("bar"): s has already been compiled} {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
		s.compile();
		s.add("bar");
	}
}

varnish v1 -errvcl {vmod re2 failure: s.compile(): s has already been compiled} {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
		s.compile();
		s.compile();
	}
}

server s1 {
	rxreq
	txresp
} -start

# .compile() is no longer required.
varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
	}

	sub vcl_deliver {
		if (s.match("foo")) {
			set resp.http.foo = "match";
		}
	}
}

client c1 {
	txreq
	rxresp 
	expect resp.status == 200
	expect resp.http.foo == "match"
} -run

# Safe to call .match() in vcl_init
varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
		s.compile();
		if (s.match("foo")) {
			new bar = re2.set();
			bar.add("bar");
			bar.compile();
		}
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		if (bar.match("bar")) {
			set resp.http.bar = "match";
		}
	}
}

client c1 {
	txreq
	rxresp 
	expect resp.status == 200
	expect resp.http.bar == "match"
} -run

varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
	}

	sub vcl_deliver {
		s.add("bar");
	}
}

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: s\.add\("bar"\): \.add\(\) may only be called in vcl_init$}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

logexpect l1 -wait

varnish v1 -vcl+backend {
	import ${vmod_re2};

	sub vcl_init {
		new s = re2.set();
		s.add("foo");
	}

	sub vcl_deliver {
		s.compile();
	}
}

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod re2 failure: s\.compile\(\): \.compile\(\) may only be called in vcl_init$}
	expect 0 = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 503
	expect resp.reason == "VCL failed"
	expect_close
} -run

logexpect l1 -wait

varnish v1 -errvcl {s set initialization: no patterns were added} {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
		new s = re2.set();
	}
}
