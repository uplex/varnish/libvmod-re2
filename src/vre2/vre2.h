/*-
 * Copyright (c) 2016 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef _VRE2_H
#define _VRE2_H

#ifdef __cplusplus

#include <map>
#include <re2/re2.h>

using namespace re2;
using std::string;

class vre2 {
private:
	RE2* re_;
	std::map<string, int> named_group;

public:
	vre2(const char *pattern, RE2::Options * const opt);
	virtual ~vre2();
	bool match(const char *subject, size_t len, int ngroups,
		   StringPiece* groups) const;
	int ngroups() const;
	int get_group(const char * const name) const;
	bool replace(string *text, const char * const rewrite) const;
	bool global_replace(string *text, const char * const rewrite) const;
	bool extract(string *out, const char * const text,
		     const char * const rewrite) const;
	int size() const;
};
#else
typedef struct vre2 vre2;
#endif

typedef enum {
	SUB = 0,
	SUBALL,
	EXTRACT
} rewrite_e;

#ifdef __cplusplus
extern "C" {
#endif

	const char *vre2_init(vre2 **vre2, const char * pattern, unsigned utf8,
			      unsigned posix_syntax, unsigned longest_match,
			      long max_mem, unsigned literal,
			      unsigned never_nl, unsigned dot_nl,
			      unsigned never_capture, unsigned case_sensitive,
			      unsigned perl_classes, unsigned word_boundary,
			      unsigned one_line);
	const char *vre2_fini(vre2 **vre2);
	size_t vre2_matchsz(void);
	const char *vre2_ngroups(vre2 *vre2, int * const ngroups);
	const char *vre2_match(vre2 *vre2, const char * const subject,
			       size_t len, int * const match, int ngroups,
			       void * const group);
	const char *vre2_capture(void *group, int refnum,
				 const char ** const capture, int * const len);
	const char *vre2_get_group(vre2 *vre2, const char * const name,
				   int * const refnum);
	const char *vre2_rewrite(vre2 *vre2, const rewrite_e mode,
				 const char * const text,
				 const char * const rewrite,
				 char * const dest, const size_t bytes,
				 int * const match, size_t * const len);
	const char *vre2_cost(vre2 *vre2, int *cost);
	const char *vre2_quotemeta(const char * const unquoted,
				   char * const dest, const size_t bytes,
				   size_t * const len);

#ifdef __cplusplus
}
#endif

#endif /* _VRE2_H */
